#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from pathlib import Path
import os
from os.path import join
import sys
import re
from argparse import ArgumentParser
from subprocess import PIPE, run, CalledProcessError
import shutil
import webbrowser
import warnings

# --- Configuration below ---

gitusername = 'askhl'
gnomedir = '/home/askhl/src/gnome'
mylang = 'da'
prettylangname = 'Danish'

authors = {'alan': 'Alan Mortensen <alanmortensen.am@gmail.com>',
           'anders': 'Anders Jenbo <anders@jenbo.dk>',
           'aputsiaq': 'Aputsiaq Janussen <aj@isit.gl>',
           'askhl': 'Ask Hjorth Larsen <asklarsen@gmail.com>',
           'charlie': 'Charlie Tyrrestrup <chalze06@gmail.com>',
           'flemming': 'Flemming Christensen <fc@stromata.dk>',
           'jacob': 'Jacob I. Christensen <jacobilsoe@gmail.com>',
           'joe': 'Joe Hansen <joedalton2@yahoo.dk>',
           'kenneth': 'Kenneth Nielsen <k.nielsen81@gmail.com>',
           'klaus': 'Klaus Rasmussen <claudius01@gmail.com>',
           'me': 'Ask Hjorth Larsen <asklarsen@gmail.com>',
           'kongstad': 'Per Kongstad <p_kongstad@op.pl>',
           'kris': 'Kris Thomsen <lakristho@gmail.com>',
           'mads': 'Mads Lundby <lundbymads@gmail.com>',
           'nicky': 'Nicky Thomassen <nicky.thomassen@gmail.com>',
           'rommedahl': 'M.P. Rommedahl <lhademmor@gmail.com>',
           'scooter': 'scootergrisen'}

# --- End of configuration ---

desc = """\
Clone, pull, commit, and push GNOME translations.

Project name and branch are deduced from filename when possible.

Typically used on multiple files with one or more of the options
--pull, --commit <author>, --push.
"""


# From pyg3t
_colors = {'blue': '0;34',
           'light red': '1;31',
           'light purple': '1;35',
           'brown': '0;33',
           'purple': '0;35',
           'yellow': '1;33',
           'dark gray': '1;30',
           'light cyan': '1;36',
           'black': '0;30',
           'light green': '1;32',
           'cyan': '0;36',
           'green': '0;32',
           'light blue': '1;34',
           'light gray': '0;37',
           'white': '1;37',
           'red': '0;31',
           'old': '1;31;41',  # To do: proper names, reorganize
           'new': '1;33;42',  # These are used by gtprevmsgdiff
           None: None}


def ansi(name):
    def color(string):
        return '\x1b[{}m{}\x1b[0m'.format(_colors[name], string)
    return color


header = ansi('yellow')
blue = ansi('blue')
red = ansi('red')
purple = ansi('purple')
yellow = ansi('yellow')
green = ansi('green')
lightblue = ansi('light blue')
lightred = ansi('light red')

def cmd(*args, **kwargs):
    print(purple(' '.join('"{}"'.format(arg) if ' ' in arg else arg
                          for arg in args)))
    return run(args, check=True, **kwargs)

def git(*args, **kwargs):
    return cmd('git', *args, **kwargs)

def status():
    return git('status')

def msgfmt(fname):
    return cmd('msgfmt', '-c', '-v', '-o', '/dev/null', fname)

def gtxml(fname):
    return cmd('gtxml', fname)

def gnome_clone(group, module):
    return git('clone', 'git@gitlab.gnome.org:{}/{}.git'.format(group,
                                                                module))

def compare(file1, file2):
    cmd('gtcompare', file1, file2)

def diff(file1, file2):
    cmd('podiff', '--full', '--color', file1, file2)


startdir = os.getcwd()

def main():
    p = ArgumentParser(description=desc)
    p.add_argument('--pull', action='store_true',
                   help='pull (with rebase) or clone modules')
    p.add_argument('--commit', metavar='AUTHOR',
                   help='commit translation in name of AUTHOR.  '
                   'Note that allowed authors are hardcoded')
    p.add_argument('--hardreset', action='store_true',
                   help='hard-reset local copy to match branch on origin')
    p.add_argument('--push', action='store_true',
                   help='push to repository (requires yes/no)')
    p.add_argument('--batch-push', action='store_true',
                   help='use with --push.  Push non-interactively.  '
                   'For the love of all that is good and holy, please verify '
                   'each file interactively with --push before running this')
    p.add_argument('files', nargs='+',
                   help='files to process, normally named '
                   '<project>.<branch>.{}.po'.format(mylang))
    #p.add_argument('--web', action='store_true',
    #               help='open corresponding sites on damned lies in web '
    #               'browser')
    p.add_argument('--branch',
                   help='commit/push to BRANCH.  In most cases the branch '
                   'is inferred from the filename.')
    #p.add_argument('--no-auto-master', action='store_true',
    #               help='do not automatically process master'
    #               'branch when processing a non-master branch')
    p.add_argument('--compare', action='store_true',
                   help='compare to existing po-file (gtcompare from pyg3t)')
    p.add_argument('--diff', action='store_true',
                   help='diff against existing po-file (podiff from pyg3t)')

    args = p.parse_args()

    for i, po_rawpath in enumerate(args.files):
        if i > 0:
            print()

        popath = os.path.abspath(po_rawpath)
        podir, pofile = os.path.split(popath)
        match = re.match(r'([^\.]+)\.(.+?)\.([^\.]+)\.po$', pofile)
        if match is None:
            raise RuntimeError('Cannot deduce project name and branch from '
                               'filename: "{}"'.format(pofile))
        module = match.group(1)

        if args.branch:
            branch = args.branch
        else:
            branch = match.group(2)

        lang = match.group(3)

        process(args, popath=popath, pofile=pofile, module=module,
                branch=branch, lang=lang)

        # if branch != 'master' and not args.no_auto_master:
        #     process(args, popath=popath, pofile=pofile, module=module,
        #             branch='master', lang=lang)


def process(args, popath, pofile, module, branch, lang):
    gitpodir = 'po'
    gitlab_group = 'GNOME'
    ishelp = module.endswith('-help')

    if ishelp:
        module = module.rsplit('-', 1)[0]

    git_repository = module

    if module == 'gnumeric-functions':
        git_repository = 'gnumeric'
        gitpodir = 'po-functions'
    elif module == 'gtk-properties':
        git_repository = 'gtk'
        gitpodir = 'po-properties'
    elif module.startswith('smuxi-'):
        git_repository = 'smuxi'
        _smuxi, smuxi_ending = module.split('-', 1)
        gitpodir = 'po-' + smuxi_ending
    elif module.startswith('gimp-') and module not in ['gimp-tiny-fu',
                                                       'gimp-gap']:
        git_repository = 'gimp'
        _gimp, gimp_ending = module.split('-', 1)
        gitpodir = 'po-' + gimp_ending
    elif module == 'cantarell-fonts':
        gitpodir = 'appstream'
        assert branch.startswith('appstream.')
        _, branch = branch.split('.', 1)
    elif module == 'locations':
        git_repository = 'libgweather'
        gitpodir = 'po-locations'
    elif module == 'gtk':
        pass
        #git_repository = 'gtk+'
    elif module == 'getting-started':
        git_repository = 'gnome-getting-started-docs'
        gitpodir = 'gnome-help'
        warnings.warn('Not quite working (Makefile/LINGUAS)')
    elif module == 'desktop-icons':
        git_repository = 'desktop-icons'
        gitlab_group = 'World/ShellExtensions'
    elif module in ['gcolor3',
                    'PasswordSafe',
                    'podcasts']:
        gitlab_group = 'World'

    pofile_basename = '{}.po'.format(mylang)

    if module == 'gnome-keysign':
        gitpodir = 'locale/da/LC_MESSAGES'
        pofile_basename = 'keysign.po'

    if ishelp:
        gitpodir = 'help/{}'.format(lang)

    special_case = (git_repository != module)

    moduledir = join(gnomedir, git_repository)
    gitpofile = join(moduledir, gitpodir, pofile_basename)

    #if args.web:
    #    raise RuntimeError('This code not properly written/tested')
    #    # Do we want this?  Untested; test and enable later
    #    webbrowser.open('https://l10n.gnome.org/languages/'
    #                    '{lang}/{module}/ui/'.format(lang=lang,
    #                                                 module=module))

    print(yellow('Processing {}'.format(pofile)))
    if ishelp:
        print(yellow('This is module documentation'))
    print(green('Module:'), lightblue(module))
    if special_case:
        print(green('   Git:'), lightblue(git_repository))
    print(green('Branch:'), lightblue(branch))
    print(green('Source:'), lightblue(moduledir))
    print(green('Target:'), lightblue(gitpofile))
    if args.commit:
        author = authors[args.commit]
        print(green('Author:'), lightblue(author))
    print()
    print(yellow('Verify file'))
    msgfmt(popath)
    print()
    print(yellow('Check xml if any'))
    gtxml(popath)
    assert lang == mylang, lang

    try:
        if os.path.exists(moduledir):
            os.chdir(moduledir)
            print(yellow('Checkout {}'.format(branch)))
            if args.pull:
                print(yellow('Pull module {}'.format(git_repository)))
                # Branch may not exist yet so we fetch before attempting
                # to check out the branch, then pull from disk
                # Actually this won't quite work if we don't have the
                # branch locally
                cmd('updateremote.py')
                git('fetch', 'origin')
                git('checkout', branch)
                git('rebase')
                #git('checkout', branch)
                #git('pull', '--rebase', '.', branch)
            else:
                git('checkout', branch)
        else:
            if args.pull:
                os.chdir(gnomedir)
                print(yellow('Clone new module {}'.format(git_repository)))
                gnome_clone(gitlab_group, git_repository)
                os.chdir(moduledir)
            else:
                raise RuntimeError('Could not clone {} for {}'
                                   .format(git_repository, pofile))

        if args.hardreset:
            print(yellow('Hardreset {} {}'.format(git_repository, branch)))
            git('reset', '--hard', 'origin/{}'.format(branch))

        internal_pofilename = '{}/{}.po'.format(gitpodir, lang)

        if args.compare:
            compare(internal_pofilename, popath)

        if args.diff:
            diff(internal_pofilename, popath)

        if args.commit:
            print(yellow('Commit translation'))
            author = authors[args.commit]
            if not os.path.exists(internal_pofilename):
                if ishelp:
                    linguas_dir = 'help'
                else:
                    linguas_dir = 'po'
                    assert linguas_dir == gitpodir
                linguas = '{}/LINGUAS'.format(linguas_dir)
                #assert 0, 'Untested code, remove assertion and verify'
                msg = red('No existing translation {}.  Press Enter to '
                          'edit {} '.format(internal_pofilename, linguas))
                input(msg)
                # The po dir might depend on GIMP/gtk+-properties etc.
                linguas = Path(linguas)
                if not linguas.exists():
                    linguas = linguas.with_name('Makefile.am')
                linguas = str(linguas)

                cmd('nano', linguas)
                git('add', linguas)
                git('commit', '-m', 'add {} to LINGUAS'.format(lang))

            Path(gitpodir).mkdir(exist_ok=True, parents=True)
            shutil.copy(popath, internal_pofilename)  # langcode
            cmd('dos2unix', internal_pofilename)
            msgfmt(internal_pofilename)
            git('add', internal_pofilename)
            commitmsg = 'Updated {} translation'.format(prettylangname)
            if special_case:
                commitmsg += ' of ' + module
            commit_args = ['commit', '-m', commitmsg]
            if args.commit != 'me':
                commit_args += ['--author', author]
            git(*commit_args)

        git('status')

        def push(pushbranch=None):
            if pushbranch is None:
                pushbranch = branch
            git('--no-pager', 'log', '--decorate', '--name-status',
                'origin/{branch}..{branch}'.format(branch=pushbranch))

            if args.batch_push:
                proceed = True
                print(lightred('BATCH PUSH TO: {} {}'
                               .format(git_repository, pushbranch)))
            else:
                confirm = input(yellow('Push to {} {} [Y/n]? '
                                       .format(git_repository,
                                               pushbranch))).lower()
                proceed = (confirm == 'y' or confirm == '')

            if proceed:
                try:
                    proc = git('push', 'origin', pushbranch, stderr=PIPE)
                    txt = proc.stderr.decode('utf8')
                    print(txt)
                except CalledProcessError as err:
                    print(err.stderr.decode('utf8'), file=sys.stderr)
                    raise
            else:
                yellow('Skipping {}, {} {}'.format(pofile, git_repository,
                                                   pushbranch))

        if args.push:
            push()

    finally:
        yellow('Leaving {}, {} {}'.format(pofile, git_repository, branch))
        #if branch != 'master':
        #    git('checkout', 'master')
        os.chdir(startdir)

if __name__ == '__main__':
    main()
