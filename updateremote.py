#!/usr/bin/env python3
import os
from subprocess import check_output, check_call

txt = check_output('git remote get-url origin'.split()).decode('ascii')

if txt.startswith('ssh'):
    modulename = txt.strip().split('/')[-1]
    newremote = 'git@gitlab.gnome.org:GNOME/{}.git'.format(modulename)
    cmd = 'git remote set-url origin {}'.format(newremote)
    print('SUBSTITUTE REMOTE:', cmd)
    check_call(cmd.split())
